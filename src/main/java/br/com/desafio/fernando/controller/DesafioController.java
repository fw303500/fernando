package br.com.desafio.fernando.controller;

import java.util.NoSuchElementException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.fernando.controller.form.ConsultaForm;
import br.com.desafio.fernando.dto.BaseADto;
import br.com.desafio.fernando.dto.BaseBDto;
import br.com.desafio.fernando.dto.BaseCDto;
import br.com.desafio.fernando.service.DesafioService;
import br.com.desafio.fernando.utils.Uteis;

@RestController
public class DesafioController {
	
	Uteis utilidades = new Uteis();
	DesafioService service= new DesafioService();
	
	@RequestMapping("/carregaBaseA")
	public ResponseEntity<BaseADto> baseA(@RequestBody ConsultaForm form) {
		try {
			BaseADto baseA = service.recuperaBaseA(form.getCpf());
			if(baseA != null) {
				return ResponseEntity.ok(baseA);
			}
		} catch (NoSuchElementException e) {
			return ResponseEntity.status(204).build();
		}
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping("/carregaBaseB")
	public ResponseEntity<BaseBDto> baseB(@RequestBody ConsultaForm form) {
		try {
			BaseBDto baseB = service.recuperaBaseB(form.getCpf());
			if(baseB != null) {
				return ResponseEntity.ok(baseB);
			}
		} catch (NoSuchElementException e) {
			return ResponseEntity.status(204).build();
		}
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping("/carregaBaseC")
	public ResponseEntity<BaseCDto> baseC(@RequestBody ConsultaForm form) { 
		try {
			BaseCDto baseC = service.recuperaBaseC(form.getCpf());
			if(baseC != null) {
				return ResponseEntity.ok(baseC);
			}
		} catch (NoSuchElementException e) {
			return ResponseEntity.status(204).build();
		}
		return ResponseEntity.notFound().build();
	}
}
