package br.com.desafio.fernando.controller.form;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class LoginForm {
	
	private String identificador;
	private String senha;
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getIdentificador() {
		return identificador;
	}
	public String getSenha() {
		return senha;
	}
	public UsernamePasswordAuthenticationToken converter() {
		return new UsernamePasswordAuthenticationToken(identificador, senha);
	}
	
	
}
