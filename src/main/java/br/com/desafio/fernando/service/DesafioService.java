package br.com.desafio.fernando.service;

import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

import br.com.desafio.fernando.dto.BaseADto;
import br.com.desafio.fernando.dto.BaseBDto;
import br.com.desafio.fernando.dto.BaseCDto;
import br.com.desafio.fernando.dto.PessoaDto;
import br.com.desafio.fernando.mock.Mock;
import br.com.desafio.fernando.model.UsuarioModel;

@Service
public class DesafioService {
	
	Mock mock = new Mock();

	public PessoaDto dadosFull(String cpf) {
		return mock.recuperaPessoa(cpf);
	}

	public BaseADto recuperaBaseA(String cpf) throws NoSuchElementException {
		return mock.recuperaBaseA(cpf);
	}

	public BaseBDto recuperaBaseB(String cpf) throws NoSuchElementException {
		return mock.recuperaBaseB(cpf);
	}

	public BaseCDto recuperaBaseC(String cpf) throws NoSuchElementException {
		return mock.recuperaBaseC(cpf);
	}

	public UsuarioModel carregaUsuario(String username) throws NoSuchElementException {
		return mock.recuperaUsuario(username);
	}

}
