package br.com.desafio.fernando.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class Uteis {
	
	public LocalDate criaData(int ano, int mes, int dia) {
		
		return LocalDate.of(ano, mes, dia);
				
	}
	
	public String retornaDataString(LocalDate data) {
		return data.format(DateTimeFormatter.ofPattern("dd/MM/yyy"));
	}

}
