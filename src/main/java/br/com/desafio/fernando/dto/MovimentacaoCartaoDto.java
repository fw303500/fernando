package br.com.desafio.fernando.dto;

import java.time.LocalDate;

public class MovimentacaoCartaoDto {
	
	
	public MovimentacaoCartaoDto(LocalDate data, String local, Long valor) {
		this.data = data;
		this.local = local;
		this.valor = valor;
	}
	private LocalDate data;
	private String local;
	private Long valor;
	
	public LocalDate getData() {
		return data;
	}
	public String getLocal() {
		return local;
	}
	public Long getValor() {
		return valor;
	}
}
