package br.com.desafio.fernando.dto;

import java.time.LocalDate;

public class BensDto {
	
	String tipo;
	String descricao;
	Long valor;
	LocalDate dataAquisicao;
	
	public BensDto(String tipo, String descricao, Long valor, LocalDate dataAquisicao) {
		super();
		this.tipo = tipo;
		this.descricao = descricao;
		this.valor = valor;
		this.dataAquisicao = dataAquisicao;
	}

	public String getTipo() {
		return tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public Long getValor() {
		return valor;
	}

	public LocalDate getDataAquisicao() {
		return dataAquisicao;
	}
	
}
