package br.com.desafio.fernando.dto;

import java.time.LocalDate;
import java.util.List;

public class PessoaDto {
	
	public PessoaDto(	String cpf, 
						String nome, 
						LocalDate nascimento, 
						String fonteRenda, 
						EnderecoDto endereco, 
						List<DividaDto> dividas, 
						List<BensDto> bens,
						LocalDate ultimaConsulta,
						List<MovimentacaoFinanceiraDto> movimentacao,
						List<MovimentacaoCartaoDto> historicoCartao) 
	{
		this.cpf = cpf;
		this.nome = nome;
		this.nascimento = nascimento;
		this.fonteRenda = fonteRenda;
		this.endereco = endereco;
		this.dividas = dividas;
		this.bens = bens;
		this.ultimaConsulta = ultimaConsulta;
		this.movimentacao = movimentacao;
		this.historicoCartao = historicoCartao;
	}
	
	/*
	 * Base A
	 * */
	private String cpf;
	private String nome;
	private EnderecoDto endereco;
	private List<DividaDto> dividas;
	
	/*
	 * Base B
	 * */
	private LocalDate nascimento;
	private String fonteRenda;
	private List<BensDto> bens;
	
	/*
	 * Base C
	 * */
	private LocalDate ultimaConsulta;
	private List<MovimentacaoFinanceiraDto> movimentacao;
	private List<MovimentacaoCartaoDto> historicoCartao;
	
		
	public LocalDate getUltimaConsulta() {
		return ultimaConsulta;
	}
	public List<MovimentacaoFinanceiraDto> getMovimentacao() {
		return movimentacao;
	}
	public List<MovimentacaoCartaoDto> getHistoricoCartao() {
		return historicoCartao;
	}
	public List<BensDto> getBens() {
		return bens;
	}
	public LocalDate getNascimento() {
		return nascimento;
	}
	public String getFonteRenda() {
		return fonteRenda;
	}
	
	public String getCpf() {
		return cpf;
	}
	public String getNome() {
		return nome;
	}
	public EnderecoDto getEndereco() {
		return endereco;
	}
	public List<DividaDto> getDividas() {
		return dividas;
	}
}
