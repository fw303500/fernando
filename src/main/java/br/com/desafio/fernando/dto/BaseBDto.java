package br.com.desafio.fernando.dto;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;

/**Classe responsável por nortear as informações que serão mostradas ao requisitante do serviço da base B.
* @author Fernando William
* @version 1.00
* @since Release de entrega
*/

public class BaseBDto {
	
	
	
	public BaseBDto(PessoaDto pessoa) {
		super();
		this.idade = calculaIdade(pessoa.getNascimento());
		this.bens = pessoa.getBens();
		this.endereco = pessoa.getEndereco();
		this.fonteRenda = pessoa.getFonteRenda();
	}

	private int idade;
	private List<BensDto> bens;
	private EnderecoDto endereco;
	private String fonteRenda;
	
	public int getIdade() {
		return idade;
	}
	public List<BensDto> getBens() {
		return bens;
	}
	public EnderecoDto getEndereco() {
		return endereco;
	}
	public String getFonteRenda() {
		return fonteRenda;
	}
	
	/**Método para calculo de idade
	* @author Fernando William
	* @param  nascimento LocalDate - data de nascimento.
	* @return int - Idade na presente data
	*/
	private int calculaIdade(LocalDate nascimento) {
		Period periodo = Period.between(nascimento, LocalDate.now());
		return periodo.getYears();
	}	

}
