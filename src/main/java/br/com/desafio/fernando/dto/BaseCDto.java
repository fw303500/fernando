package br.com.desafio.fernando.dto;

import java.time.LocalDate;
import java.util.List;

public class BaseCDto {
	
	
	public BaseCDto(PessoaDto pessoa) {
		this.ultimaConsulta = pessoa.getUltimaConsulta();
		this.movimentacao = pessoa.getMovimentacao();
		this.ultimaTransacaoCartao = retornaUltima(pessoa.getHistoricoCartao());
	}
	
	private LocalDate ultimaConsulta;
	private List<MovimentacaoFinanceiraDto> movimentacao;
	private MovimentacaoCartaoDto ultimaTransacaoCartao;
	
	public LocalDate getUltimaConsulta() {
		return ultimaConsulta;
	}
	public List<MovimentacaoFinanceiraDto> getMovimentacao() {
		return movimentacao;
	}
	public MovimentacaoCartaoDto getHistoricoCartao() {
		return ultimaTransacaoCartao;
	}
	
	private MovimentacaoCartaoDto retornaUltima(List<MovimentacaoCartaoDto> historicoCartao2) {
		return historicoCartao2.stream().sorted((d1, d2) -> d2.getData().compareTo(d1.getData())).findFirst().get(); 
	}

}
