package br.com.desafio.fernando.dto;

import java.time.LocalDate;

public class DividaDto {
	
	public DividaDto(Long valorInicial, Long valorAtualizado, LocalDate dataDaDivida, String empresaCredora) {
		this.valorInicial = valorInicial;
		this.ValorAtualizado = valorAtualizado;
		this.dataDaDivida = dataDaDivida;
		this.empresaCredora = empresaCredora;
	}
	
	private Long valorInicial;
	private Long ValorAtualizado;
	private LocalDate dataDaDivida;
	private String empresaCredora;
	public Long getValorInicial() {
		return valorInicial;
	}
	public void setValorInicial(Long valorInicial) {
		this.valorInicial = valorInicial;
	}
	public Long getValorAtualizado() {
		return ValorAtualizado;
	}
	public void setValorAtualizado(Long valorAtualizado) {
		ValorAtualizado = valorAtualizado;
	}
	public LocalDate getDataDaDivida() {
		return dataDaDivida;
	}
	public void setDataDaDivida(LocalDate dataDaDivida) {
		this.dataDaDivida = dataDaDivida;
	}
	public String getEmpresaCredora() {
		return empresaCredora;
	}
	public void setEmpresaCredora(String empresaCredora) {
		this.empresaCredora = empresaCredora;
	}
	
}
