package br.com.desafio.fernando.dto;

import java.time.LocalDate;

public class MovimentacaoFinanceiraDto {
	
	
	
	public MovimentacaoFinanceiraDto(LocalDate data, String descricao, Long valor) {
		this.data = data;
		this.descricao = descricao;
		this.valor = valor;
	}
	
	LocalDate data;
	String descricao;
	Long valor;
	
	public LocalDate getData() {
		return data;
	}
	public String getDescricao() {
		return descricao;
	}
	public Long getValor() {
		return valor;
	}	
}
