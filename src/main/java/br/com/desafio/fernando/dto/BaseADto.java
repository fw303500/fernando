package br.com.desafio.fernando.dto;

import java.util.List;

public class BaseADto {
			
	public BaseADto(PessoaDto pessoa) {
		super();
		this.cpf = pessoa.getCpf();
		this.nome = pessoa.getNome();
		this.endereco = pessoa.getEndereco();
		this.dividas = pessoa.getDividas();
	}
	
	private String cpf;
	private String nome;
	private EnderecoDto endereco;
	private List<DividaDto> dividas;
	
	
	public String getCpf() {
		return cpf;
	}
	public String getNome() {
		return nome;
	}
	public EnderecoDto getEndereco() {
		return endereco;
	}
	public List<DividaDto> getDividas() {
		return dividas;
	}
	
}
