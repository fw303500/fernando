package br.com.desafio.fernando.model;

import org.springframework.security.core.GrantedAuthority;

public class PermissaoModel implements GrantedAuthority{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PermissaoModel(String nome) {
		this.nome = nome;
	}

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String getAuthority() {
		return nome;
	}
	
	
}
