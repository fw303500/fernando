package br.com.desafio.fernando.mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.security.core.userdetails.UserDetails;

import br.com.desafio.fernando.dto.BaseADto;
import br.com.desafio.fernando.dto.BaseBDto;
import br.com.desafio.fernando.dto.BaseCDto;
import br.com.desafio.fernando.dto.BensDto;
import br.com.desafio.fernando.dto.DividaDto;
import br.com.desafio.fernando.dto.EnderecoDto;
import br.com.desafio.fernando.dto.MovimentacaoCartaoDto;
import br.com.desafio.fernando.dto.MovimentacaoFinanceiraDto;
import br.com.desafio.fernando.dto.PessoaDto;
import br.com.desafio.fernando.model.PermissaoModel;
import br.com.desafio.fernando.model.UsuarioModel;
import br.com.desafio.fernando.utils.Uteis;

//A presente classe foi criada apenas para não ser necessário a geração e integração com
//bases de dados, por se tratar apenas de uma POC
public class Mock {
	
	Uteis utilidades = new Uteis();
	
	List <PessoaDto> pessoa = populaPessoa();
	List <UsuarioModel> usuario = populaUsuario();
	

	private List<PessoaDto> populaPessoa() {
		List<PessoaDto> listTemp = new ArrayList<>();
	
		listTemp.add(new PessoaDto("07199451008", "Teste do Desafio", utilidades.criaData(1980,  12, 26), "Comerciante",
				populaEndereco("80.800-000", "Rua do teste", "300", "Base A"), 
				populaDivida(), 
				populaBens(),
				LocalDate.now(),
				populaMovimentacao(),
				populaHistoricoCartao()));
		
		listTemp.add(new PessoaDto("54357721091", "Fulano de Tal", utilidades.criaData(1954,  5, 10), "Analista",
				populaEndereco("81.780-000", "Rua do Fulano", "555", "Base B"), 
				populaDivida(), 
				populaBens(),
				LocalDate.now(),
				populaMovimentacao(),
				populaHistoricoCartao()));
		
		listTemp.add(new PessoaDto("17411833088", "Ciclano de Tal", utilidades.criaData(1959,  4, 9), "Do Lar",
				populaEndereco("85.320-280", "Rua do Fulano", "351", "Base C"), 
				populaDivida(), 
				populaBens(),
				LocalDate.now(),
				populaMovimentacao(),
				populaHistoricoCartao()));
		
		listTemp.add(new PessoaDto("27151002050", "Zequinha da Silva", utilidades.criaData(1982,  9, 29), "Veterinaro",
				populaEndereco("85.320-280", "Rua Bossa Nova", "171", "Lote 3"), 
				populaDivida(), 
				populaBens(),
				LocalDate.now(),
				populaMovimentacao(),
				populaHistoricoCartao()));
		
		return listTemp;
		
	}

	private List<UsuarioModel> populaUsuario() {
		List<UsuarioModel> lista = new ArrayList<UsuarioModel>();
		
		lista.add(new UsuarioModel("111111111", "$2a$10$b0ddcMyayevQB.3yof97XOuyCnr1kGyMiw7IN5HYVFaVsaTRynmAi", Stream.of(new PermissaoModel("BaseA"), new PermissaoModel("BaseB")).collect(Collectors.toList())));
		lista.add(new UsuarioModel("222222222", "$2a$10$Y6NEhaHyHj.Q.Ce/QkY57O1VCmkx./ljzimkEw5bnza3lr4638cX2", Stream.of(new PermissaoModel("BaseB")).collect(Collectors.toList())));
		lista.add(new UsuarioModel("333333333", "$2a$10$E9bZEeORSTIU11eDXnn/zuL/1gnWQRntM9B6nIo3jU1mm..Z2DIHK"));
		return lista;
	}

	private List<MovimentacaoCartaoDto> populaHistoricoCartao() {
		List<MovimentacaoCartaoDto> lista = new ArrayList<MovimentacaoCartaoDto>();
		lista.add(new MovimentacaoCartaoDto(utilidades.criaData(2021, 01, 12), "Loja do Shopping", 650L));
		lista.add(new MovimentacaoCartaoDto(utilidades.criaData(2021, 03, 21), "Lanchonete", 50L));
		lista.add(new MovimentacaoCartaoDto(utilidades.criaData(2021, 04, 13), "Compra Internet", 1250L));
		return lista;
	}

	private List<MovimentacaoFinanceiraDto> populaMovimentacao() {
		List<MovimentacaoFinanceiraDto> lista = new ArrayList<MovimentacaoFinanceiraDto>();
		lista.add(new MovimentacaoFinanceiraDto(utilidades.criaData(2021, 02, 04), "Compra", 100L));
		lista.add(new MovimentacaoFinanceiraDto(utilidades.criaData(2021, 01, 12), "Transferencia", 53L));
		lista.add(new MovimentacaoFinanceiraDto(utilidades.criaData(2021, 04, 04), "Pagamento", 187L));
		return lista;
	}

	private List<BensDto> populaBens() {
		List<BensDto> lista = new ArrayList<BensDto>();
		lista.add(new BensDto("Imovel", "Casa", 1000000L, utilidades.criaData(2010,  5, 10)));
		lista.add(new BensDto("Automovel", "Carro", 40000L, utilidades.criaData(2015,  3, 15)));
		lista.add(new BensDto("Imovel", "Sobrado", 900000L, utilidades.criaData(2020,  2, 20)));
		return lista;
	}

	private List<DividaDto> populaDivida() {
		List<DividaDto> lista = new ArrayList<DividaDto>();
		lista.add(new DividaDto(1000L, 1100L, utilidades.criaData(2020,  3, 10), "Empresa Credora 1"));
		lista.add(new DividaDto(500L, 700L, utilidades.criaData(2018,  2, 01), "Empresa Credora 2"));
		lista.add(new DividaDto(1300L, 1400L, utilidades.criaData(2019,  12, 13), "Empresa Credora 3"));
		lista.add(new DividaDto(1000L, 1100L, utilidades.criaData(2005,  7, 18), "Empresa Credora 4"));
		lista.add(new DividaDto(1000L, 1100L, utilidades.criaData(2007,  4, 27), "Empresa Credora 5"));
		
		return lista;
	}

	private EnderecoDto populaEndereco(String cep, String rua, String numero, String compl) {
		return new EnderecoDto(cep, rua, numero, compl);
	}



	public PessoaDto recuperaPessoa(String cpf) {
		return this.pessoa.stream().filter(p -> p.getCpf().equals(cpf)).findFirst().get();
	}

	public BaseADto recuperaBaseA(String cpf) throws NoSuchElementException{
		return new BaseADto(this.pessoa.stream().filter(p -> p.getCpf().equals(cpf)).findFirst().get());
	}

	public BaseBDto recuperaBaseB(String cpf) throws NoSuchElementException {
		return new BaseBDto(this.pessoa.stream().filter(p -> p.getCpf().equals(cpf)).findFirst().get());
	}

	public BaseCDto recuperaBaseC(String cpf) throws NoSuchElementException{
		return new BaseCDto(this.pessoa.stream().filter(p -> p.getCpf().equals(cpf)).findFirst().get());
	}

	public UsuarioModel recuperaUsuario(String username) throws NoSuchElementException{
		return this.usuario.stream().filter(p -> p.getIdentificador().equals(username)).findFirst().get();
	}
	
}
