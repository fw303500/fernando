package br.com.desafio.fernando.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.desafio.fernando.model.UsuarioModel;
import br.com.desafio.fernando.service.DesafioService;

public class AutenticacaoViaTokenFilter extends OncePerRequestFilter {
	
	private TokenService tokenService;
	
	private DesafioService desafioService;
		
	public AutenticacaoViaTokenFilter(TokenService tokenService, DesafioService desafioService) {
		this.tokenService = tokenService;
		this.desafioService = desafioService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String token = recuperarToken(request);
		boolean valido = tokenService.validaToken(token);
		if (valido) {
			autenticar(token);
		}
		
		filterChain.doFilter(request, response);
	}

	private void autenticar(String token) {
		String identificacao = tokenService.recuperaUsuario(token);
		UsuarioModel usuario = desafioService.carregaUsuario(identificacao);
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
	}

	private String recuperarToken(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		
		if (token == null || token.isEmpty() || !token.startsWith("Bearer ")) {
			return null;
		}
		return token.substring(7, token.length());
	}

}
