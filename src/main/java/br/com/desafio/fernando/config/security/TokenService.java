package br.com.desafio.fernando.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.desafio.fernando.model.UsuarioModel;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {
	
	@Value("${desafio.jwt.expira}")
	private String expira;
	
	@Value("${desafio.jwt.senha}")
	private String senhaCritp;

	public String gerarToken(Authentication authenticate) {
		UsuarioModel usuario = (UsuarioModel) authenticate.getPrincipal();
		Date hoje = new Date();
		Date expiraData = new Date(hoje.getTime() + Long.parseLong(expira));
		
		return Jwts.builder()
				.setIssuer("Desafio Fernando")
				.setSubject(usuario.getIdentificador())
				.setIssuedAt(hoje)
				.setExpiration(expiraData)
				.signWith(SignatureAlgorithm.HS256, senhaCritp)
				.compact();
	}

	public boolean validaToken(String token) {
		
		try {
			Jwts.parser().setSigningKey(this.senhaCritp).parseClaimsJws(token);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}

	public String recuperaUsuario(String token) {
		 return Jwts.parser().setSigningKey(this.senhaCritp).parseClaimsJws(token).getBody().getSubject();
	}

}
