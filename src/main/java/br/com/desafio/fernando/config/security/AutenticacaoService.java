package br.com.desafio.fernando.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.desafio.fernando.model.UsuarioModel;
import br.com.desafio.fernando.service.DesafioService;

@Service
public class AutenticacaoService implements UserDetailsService{
	
	@Autowired
	private DesafioService service;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UsuarioModel usuario = service.carregaUsuario(username);
		
		if (usuario != null) {
			return usuario;
		}
		
		throw new UsernameNotFoundException("Identificador não cadastrado!"); 
	}

}
